---
title: "constexpr C++ is not constexpr C"
document: D1447R0, ISO/IEC JTC1 SC22 WG21
date: 2019-01-20
audience: SG7/Evolution/Library Evolution
author:
  - name: Matúš Chochlík
    email: <chochlik@gmail.com>
  - name: Axel Naumann
    email: <axel@cern.ch>
  - name: David Sankel
    email: <dsankel@bloomberg.net>
toc: true
toc-depth: 4

references:
  - id: P1240R0
    citation-label: P1240R0
    title: "Scalable Reflection in C++"
    author:
      - family: Sutton
        given: Andrew
      - family: Vali
        given: Faisal
      - family: Vandevoorde
        given: Daveed
    issued:
      year: 2018
    URL: https://wg21.link/P1240R0

  - id: N4766
    citation-label: N4766
    title: "Working Draft, C++ Extensions for Reflection"
    issued:
      year: 2018
    URL: https://wg21.link/n4766

  - id: P0953R1
    citation-label: P0953R1
    title: "`constexpr reflexpr`"
    author:
      - family: Chochlík
        given: Matúš
      - family: Naumann
        given: Axel
      - family: Sankel
        given: David
    issued:
      year: 2018
    URL: https://wg21.link/P0953R1

  - id: constexpr-perf-stateless
    citation-label: constexpr-perf-stateless
    title: "Stateless OO-`constexpr` benchmark"
    author:
      family: Naumann
      given: Axel
    issued:
      year: 2019
    URL: https://bitbucket.org/karies/constexpr-perf/src/master/ 

  - id: constexpr-perf-stateful
    citation-label: constexpr-perf-stateful
    title: "Stateful OO-`constexpr` benchmark"
    author:
      family: Chochlík
      given: Matúš
    issued:
      year: 2019
    URL: https://github.com/matus-chochlik/mirror/tree/develop/benchmark/constexpr-perf

  - id: clang-stateful
    citation-label: clang-stateful
    title: "Clang-modifications for a rudimentary, naive stateful reflection implementation."
    author:
      family: Chochlík
      given: Matúš
    issued:
      year: 2019
    URL: https://github.com/matus-chochlik/clang/tree/reflexpr
---


# Introduction

Reflection is moving towards `constexpr` value-based notation,
becoming the first of its kind in the stdlib.
We as a community need to decided whether we want `constexpr` value-based libraries
to follow fundamental C++ design patterns, despite concerns raised about their performance in
current compilers.
The authors believe that `constexpr` libraries designed as a collection of C-style
free functions without strong types would be a disservice to the community, and
would contradict everything C++ has tought us.
We provide explanations and numbers to support this bold statement.
 
# Members or Free Functions? Syntax and typesafety

The discussion boils down to the following pseudo-Tony-Table:

::: tonytable
### Free-function-style [@P1240R0]
```cpp
auto str_m = reflexpr(std::string);
auto mems
  = std::reflect::get_member_types(str_m);
auto first = std::reflect::at(mems, 0);
auto name = std::reflect::name(first);
```
### Our preferred OO-style
```cpp
auto str_m = reflexpr(std::string);
auto mems = str_m.get_member_types();

auto first = mems[0];
auto name = first.name();
```
:::

People appreciate the object-oriented, type-safe programming style, still today.
Many of the newest, fanciest libraries employ these patterns.
They strive for clean, conceptually simple value semantics - which shows the role OO-style programming has today.
Thank you, C++!

But compilers were SLOW, in the 90s, compared to C. Compiling C++ is still slower than compiling C.
Yet, C++ survives well.
Why should we give this up for `constexpr`-based libraries?
Why would different arguments hold for `constexpr`-based libraries than for regular ones?
The authors believe in the vendors' capability to accelerate the relatively new `constexpr` compilation, based on users' demand.
And our numbers below show that acceleration of `constexpr` compilation might not even be needed!

One of the motivations of substituting the Reflection TS's template meta-programming style [@N4766] by
`constexpr`-based interfaces is the simplicity of code, becoming more natural to read:
templates require the operations to the left of their operand.
Free-function-style `constexpr` programming keeps this "inverse Polish notation", while the OO-style
we prefer makes this very readable (natural?) code.
The following Tony Table demonstrates this:

::: tonytable
### Reflection-TS [@N4766]
```cpp
using X_m = get_scope_t<
  get_type_t<
    reflexpr(some_var)
    >
  >;
```

### Our preferred notation
```cpp
auto X_m = reflexpr(some_var)
  .get_type()
  .get_scope();
```
:::

A major bonus of C++ is typesafety: you can drive a car, not a cat, and the compiler will tell you.
[@P1240R0] instead suggests to tear down reflection typesafety, using a fundmental, opaque type for everything.
Without typesafety, [@P1240R0] now needs to introduce an "invalid" state of the untyped reflection "object",
the resulting of calling for instance `get_members` on `reflexpr(int)`.

The object-oriented, typesafe approach of [@P0953R1] does not even offer this operation: `reflexpr(int)` yields a `Type`
not a `Record`, and only th latter offers the interface `get_members()`.
This is what we are used to (and love!) with C++.

To clarify, we could have written the first Tony Table as follows, with an implicit `using namespace std::meta` and `using namespace std::reflect`:

::: tonytable
### Free-function-style [@P1240R0]
```cpp
template <class T>
constexpr std::string getName() {
  info str_m = reflexpr(T);
  if (!is_valid(str_m))
    throw std::logic_error("T is invalid for reflection");
  info mems = get_member_types(str_m);
  if (!is_valid(str_m))
    throw std::logic_error("T does not have members");
  info first = at(mems, 0);
  if (!is_valid(str_m))
    throw std::logic_error("Zero members");
  constexpr std::string name = name(first);
  // handle invalid operation?
  return name;
}
```

---

### Our preferred OO-style
```cpp
template <class T>
constexpr std::string getName() {
  Class str_m = reflexpr(T);
  Seq<RecordMember> mems = str_m.get_member_types();
  RecordMember first = mems[0];
  return first.name();
}
```
:::


# Performance

During the San Diego discussion of [@P0953R1] and [@P1240R0], some vendors raised concern about
the (compile-time) performance implication of [@P0953R1]'s object-oriented notation.
To measure the impact, we have conducted two benchmarks, both using clang. They provide an estimate
on the upper and lower boundary of the performance cost of the object-oriented `constexpr` notation.

## Stateless

The small benchmark [@constexpr-perf-stateless] generates constexpr evaluations of 10000 functions.
Each provokes numerous instances of name lookup. The return values of all functions are "stateless"
in that they do not require context; the compiler will immediately evaluate them to a constant value.

```cpp
// p0953_head.cxx
#include <array>
enum Dummy{a, b, c};

struct Enumerator {
   constexpr std::array<char, 128> name() { return {}; }
   constexpr int value() { return 42; }
};
struct Enumeration {
   constexpr std::array<Enumerator, 3> enumerators() { return {}; }
};

constexpr Enumeration reflexpr() { return {}; }
```

```cpp
// p0953_one.cxx; cloned 10000 times, replacing '@' with 1, 2,...
template <class T>
constexpr std::array<char, 128> get_name_@(int v) {
   for (Enumerator e: reflexpr(/*T*/).enumerators()) {
      if (e.value() == v)
         return e.name();
   }
   return {};
}

constexpr auto eval@ = get_name_@<Dummy>(1);
```

This is compared to a [@P1240R0]-style set of free functions, doing the same operations.

```cpp
// p1240_head.cxx:
#include <array>
enum Dummy{a, b, c};

struct Info {};

constexpr std::array<Info,3> enumerators(Info) { return {}; }
constexpr int value(Info) { return 42; }
constexpr std::array<char, 128> name(Info) { return {}; }

constexpr Info reflexpr() { return {}; }
```

```cpp
// p1240_one.cxx
template <class T>
constexpr std::array<char, 128> get_name_@(int v) {
   for (Info e: enumerators(reflexpr(/*T*/))) {
      if (value(e) == v)
      return name(e);
   }
   return {};
}

constexpr auto eval@ = get_name_@<Dummy>(1);
```

The source file generated by a concatenation of the respective `..._head.cxx` and 10000 clones of its
`..._one.cxx`, where '@' is replaced by a running number, is then compiled on a MacBook 2014, 2.5 GHz Intel Core i7, with `-fsyntax-only`
with
```bash
$ clang --version
Apple LLVM version 10.0.0 (clang-1000.11.45.5)
Target: x86_64-apple-darwin18.2.0
Thread model: posix
```

The resulting timings are

::: tonytable
### free-function, p1240
```
real  0m2.969s
user  0m2.858s
sys   0m0.100s
```

### object-oriented, p0953
```
real  0m2.702s
user  0m2.592s
sys   0m0.101s
```
:::

As one can see, the difference is within the range of "random" fluctuations.


## Stateful

A more complete benchmark modifies recent clang from Wed, Jan 2, 2019, [@clang-stateful] to enable a
comparison where `constexpr`-state is provided through compiler intrinsics, and passed along through
 either object-oriented of free-function coding style.
The code to be benchmarked [@constexpr-perf-stateful] exercises similar functionality and uses a similar setup
as the code for the stateless benchmark.

::: tonytable
### free-function, p1240

```
real    0m2,674s
user    0m2,649s
sys    0m0,024s
```

### object-oriented, p0953

```
real    0m4,020s
user    0m3,987s
sys    0m0,032s
```
:::

The object-oriented approach is naive in that it uses a dedicated `this` pointer that needs to be evaluated
to determine the (opaque) pointer value to the AST-node. On an Intel Core i5-2400 CPU @ 3.10GHz, this
extra step costs about 50% in performance compared to the free function approach, where the (opaque) pointer
value to the AST-node is the `Info` node itself.
A smarted implementation of the object-oriented style could probably map the `this` pointer of the `constexpr`
object to the internal AST-node for reflection objects, alleviating most of the overhead.


## Conclusion

We can show that the lookup performance is approximately equal. On the other hand, the time to pass context / AST-node
pointers depends on the implementation. Be believe that this can be optimized in several ways.
Our benchmarks thus provide a lower limit of the overhead of the object-oriented coding style of 0%, and an upper
value of the overhead of 48%.

Note that this benchmark is purely synthetical; code that uses reflection operations in about 100% of its code
will be extremely rare. The actual overhead in real code will thus be only a fraction of whatever overhead we
would incur from object-oriented programming style.


# Summary

Object-oriented design is simple to reason about and easy to write.
It fits naturally into C++ and its focus on values, type-safety, and conceptual abstractions.

A collection of free functions suffers similar notational issues as template meta-programming does,
where the operation is *followed* by the object it is operating on: `at(coll, idx)` instead of `coll.get(idx)`.
In San Diego, many agree that object-oriented `constexpr`-libraries are preferred in principle, 